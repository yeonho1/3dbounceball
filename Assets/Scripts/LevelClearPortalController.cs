﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelClearPortalController : MonoBehaviour
{

    public string nextLevelName;

    // Update is called once per frame
    public void NextLevel()
    {
        SceneManager.LoadScene(nextLevelName);
    }
}
