﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointController : MonoBehaviour
{
    public Material activated;
    public Material deactivated;
    public Renderer rend;
    // Start is called before the first frame update
    void Start()
    {
        if (this.deactivated == null) {
            this.deactivated = Resources.Load("YELLOW", typeof(Material)) as Material;
        }
        if (this.activated == null) {
            this.activated = Resources.Load("GREEN", typeof(Material)) as Material;
        }
        if (this.rend == null) {
            this.rend = GetComponent<Renderer>();
        }
        this.rend.material = deactivated;
    }

    public void Deactivate() {
        this.rend.material = deactivated;
    }

    public void Activate() {
        this.rend.material = activated;
    }
}
