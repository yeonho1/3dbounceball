﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    public ParticleSystem particle;

    void Start() {
        if (particle == null) {
            particle = GetComponent<ParticleSystem>();
        }
    }

    void Update() {
        if(gameObject.activeInHierarchy == false) {
            gameObject.SetActive(true);
        }
    }

    void StopParticle() {
        particle.Stop();
    }

    public void PlayParticle() {
        particle.Play();
        Invoke("StopParticle", 1);
    }
}
