﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnController : MonoBehaviour
{
    public ParticleSystem particle;

    void Start() {
        if (particle == null) {
            particle = GetComponent<ParticleSystem>();
        }
    }

    public void PlayParticle() {
        particle.Play();
    }
}
