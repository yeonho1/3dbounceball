﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoving : MonoBehaviour
{
    private Rigidbody rigidbody;
    private float jumpSpeed = 350.0f;
    private float speed = 1500.0f;
    private float rotationSpeed = 1.5f;
    public Vector3 spawnPoint = new Vector3(0, 12, 0);
    private GameObject deathParticle;
    private GameObject respawnParticle;
    private GameObject showerParticle;
    private AudioSource explosionAudio;
    private AudioSource coinAudio;
    private bool isInvincible = false;
    private GameObject checkpoint = null;
    // Start is called before the first frame update
    void Start()
    {
        this.rigidbody = GetComponent<Rigidbody>();
        this.deathParticle = GameObject.FindGameObjectWithTag("PlayerDieEffect");
        this.respawnParticle = GameObject.FindGameObjectWithTag("PlayerRespawnEffect");
        this.showerParticle = GameObject.FindGameObjectWithTag("PlayerSplashEffect");
        this.explosionAudio = transform.FindChild("ExplodeSoundEffect").GetComponent<AudioSource>();
        this.coinAudio = transform.FindChild("CoinSoundEffect").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        this.rigidbody.velocity = new Vector3(0, this.rigidbody.velocity.y, 0);
        if (Input.GetKey(KeyCode.W))
        {
            this.rigidbody.AddForce(transform.forward * speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            this.rigidbody.AddForce(transform.forward * -speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            this.rigidbody.AddForce(transform.right * -speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            this.rigidbody.AddForce(transform.right * speed * Time.deltaTime);
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(new Vector3(0, -this.rotationSpeed, 0));
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(new Vector3(0, this.rotationSpeed, 0));
        }
        if (Input.GetKey(KeyCode.R)) {
            InvokeDeathEvent();
        }
        if (transform.position.y <= -10) {
            InvokeDeathEvent();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject block = collision.gameObject;
        if(block.tag == "Map")
        {
            if (
                transform.position.y - block.transform.position.y >= transform.localScale.y - 0.05
            ) {
                this.rigidbody.velocity = new Vector3(
                    this.rigidbody.velocity.x,
                    this.jumpSpeed * Time.deltaTime,
                    this.rigidbody.velocity.z
                );
            }
        } else if(block.tag == "Death")
        {
            InvokeDeathEvent();
        } else if(block.tag == "Checkpoint")
        {
            bool playAudio = true;
            if (this.checkpoint != null) {
                if (this.checkpoint.transform.position == block.transform.position) {
                    playAudio = false;
                }
                this.checkpoint.GetComponent<CheckpointController>().Deactivate();
            }
            if (playAudio == true) {
                this.coinAudio.Play();
            }
            block.GetComponent<CheckpointController>().Activate();
            this.checkpoint = block;
            if (
                transform.position.y - block.transform.position.y >= transform.localScale.y - 0.05
            ) {
                this.rigidbody.velocity = new Vector3(
                    this.rigidbody.velocity.x,
                    this.jumpSpeed * Time.deltaTime,
                    this.rigidbody.velocity.z
                );
            }
        }
    }

    private void InvokeDeathEvent() {
        if (this.isInvincible == true) {
            return;
        }
        this.isInvincible = true;
        this.rigidbody.isKinematic = true;
        this.deathParticle.GetComponent<ExplosionController>().PlayParticle();
        this.explosionAudio.Play(0);
        Invoke("Hide", 0.1f);
        Invoke("EnableGravity", 1.5f);
        Invoke("TeleportToSpawnPoint", 1.5f);
        Invoke("Show", 1.5f);
        Invoke("RespawnEffect", 1.5f);
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Portal") {
            transform.position = other.gameObject.GetComponent<PortalController>().teleportPosition;
        } else if (other.gameObject.tag == "NextLevel") {
            other.gameObject.GetComponent<LevelClearPortalController>().NextLevel();
        }
    }

    void TeleportToSpawnPoint() {
        if (checkpoint != null) {
            Vector3 spawn = this.checkpoint.transform.position;
            spawn.y += 5;
            transform.position = spawn;
        } else {
            transform.position = this.spawnPoint;
        }
    }

    void Hide() {
        gameObject.GetComponent<Renderer>().enabled = false;
    }

    void Show() {
        gameObject.GetComponent<Renderer>().enabled = true;
    }

    void RespawnEffect() {
        this.respawnParticle.GetComponent<RespawnController>().PlayParticle();
        Invoke("UnInvincible", 2);
    }

    void UnInvincible() {
        this.isInvincible = false;
    }

    void EnableGravity() {
        this.rigidbody.useGravity = true;
        this.rigidbody.isKinematic = false;
    }
}
